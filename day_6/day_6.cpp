#include <string>

#include <iostream>
#include <iterator>
#include <fstream>
#include <sstream>

#include <algorithm>
#include <vector>

#include <math.h>


using namespace std;

// #define STAR_1


int main(int argc, char *argv[])
{
	string buffer;
	string numList;
		
	std::ifstream infile(argv[1]);
	vector<long long> time;
	vector<long long> dist;

	long endProduct = 1;
	/* Reading time */
	{
		getline(infile, buffer);


#ifdef STAR_1
		stringstream ss(buffer);
#else
		string cleanedBuffer;
		std::remove_copy(buffer.begin(), buffer.end(), std::back_inserter(cleanedBuffer), ' ');
		stringstream ss(cleanedBuffer);
#endif // STAR_1

		getline(ss, numList, ':'); /* Get the line ID out */

		std::copy(std::istream_iterator<long long>(ss), std::istream_iterator<long long>(), std::back_inserter(time));
	}

	/* Reading distances */
	{
		getline(infile, buffer);

#ifdef STAR_1
		stringstream ss(buffer);
#else
		string cleanedBuffer;
		std::remove_copy(buffer.begin(), buffer.end(), std::back_inserter(cleanedBuffer), ' ');
		stringstream ss(cleanedBuffer);
#endif // STAR_1

		getline(ss, numList, ':'); /* Get the line ID out */
		std::copy(std::istream_iterator<long long>(ss), std::istream_iterator<long long>(), std::back_inserter(dist));
	}


	/* Solving 2nd degree problem */
	for(uint i = 0; i < time.size(); i++)
	{
		const auto &t = time.at(i);
		const auto &d = dist.at(i);
		int Xmin = std::floor((time.at(i) - sqrt(t*t - 4.0 * d)) / 2) + 1;
		int Xmax = std::ceil((time.at(i) + sqrt(t*t - 4.0 * d)) / 2) - 1;

		endProduct *= (Xmax - Xmin + 1);
	}

	cout << "Total product of solutions : " << endProduct << endl;

	return 0;
}

