#include <string>

#include <iostream>
#include <fstream>

#include <algorithm>

using namespace std;


struct pipe_map
{
	size_t width;
	size_t height;

	char *data;
	char *extended_data;
	int *weighted_data;

	pipe_map(size_t width, size_t height, std::istream &input) : width(width), height(height)
	{
		size_t line = 0;
		data = new char[width * height];
		extended_data = new char[width * 3 * height * 3]; /* Each single cell becomes a 3*3 cell */
		weighted_data = new int[width * height];

		for(size_t i = 0; i < width * height; i++)
			weighted_data[i] = -1;


		/* Translate input into data */
		do
		{
			string buffer;
			getline(input, buffer);

			if(buffer.length() < width)
				continue;

			for(size_t i = 0; i < width; i++)
			{
				data[line * width + i] = buffer[i];
			}
			line++;

		} while (!input.eof());

		/* Parse data to put weights on data */
		for(size_t line = 0; line < height; line++)
		{
			for (size_t col = 0; col < width; col++)
			{
				if(data[line * width + col] == 'S') /* Found the start */
				{
					update_map(0, line, col);
				}
			}
		}

		create_extended();
		mark_openings();
	}

	void create_extended()
	{
		/* Populating the extended map, to check for enclosed parts */
		for(size_t line = 0; line < height; line++)
		{
			for (size_t col = 0; col < width; col++)
			{
				/* Default values are all '.'*/
				for (int i = 0; i < 3; i++)
				{
					for (int j = 0; j < 3; j++)
					{
						extended_data[(3 * line + i) * (3 * width) + (3 * col + j)] = '.';
					}
				}

				if(weighted_data[line * width + col] < 0)
				{
					continue; /* We first only translate the loop */
				}

				switch (data[line * width + col])
				{
				case 'S':
					for (int i = 0; i < 3; i++)
					{
						for (int j = 0; j < 3; j++)
						{
							extended_data[(3 * line + i) * (3 * width) + (3 * col + j)] = 'S';
						}
					}
					break;
				case '|':
					extended_data[(3 * line) * (3 * width) + (3 * col + 1)] = '|';
					extended_data[(3 * line + 1) * (3 * width) + (3 * col + 1)] = '|';
					extended_data[(3 * line + 2) * (3 * width) + (3 * col + 1)] = '|';
					break;
				case '-':
					extended_data[(3 * line + 1) * (3 * width) + (3 * col)] = '-';
					extended_data[(3 * line + 1) * (3 * width) + (3 * col + 1)] = '-';
					extended_data[(3 * line + 1) * (3 * width) + (3 * col + 2)] = '-';
					break;
				case 'L':
					extended_data[(3 * line) * (3 * width) + (3 * col + 1)] = '|';
					extended_data[(3 * line + 1) * (3 * width) + (3 * col + 1)] = 'L';
					extended_data[(3 * line + 1) * (3 * width) + (3 * col + 2)] = '-';
					break;
				case 'J':
					extended_data[(3 * line) * (3 * width) + (3 * col + 1)] = '|';
					extended_data[(3 * line + 1) * (3 * width) + (3 * col + 1)] = 'J';
					extended_data[(3 * line + 1) * (3 * width) + (3 * col)] = '-';
					break;
				case '7':
					extended_data[(3 * line + 1) * (3 * width) + (3 * col)] = '-';
					extended_data[(3 * line + 1) * (3 * width) + (3 * col + 1)] = '7';
					extended_data[(3 * line + 2) * (3 * width) + (3 * col + 1)] = '|';
					break;
				case 'F':
					extended_data[(3 * line + 1) * (3 * width) + (3 * col + 2)] = '-';
					extended_data[(3 * line + 1) * (3 * width) + (3 * col + 1)] = 'F';
					extended_data[(3 * line + 2) * (3 * width) + (3 * col + 1)] = '|';
					break;
				
				default:
					break;
				}
			}
		}

		/* Marking open areas, I.E which can be accessed from the border */
		for (size_t i = 0; i < 3 * width; i++)
		{
			mark_exits(0, i);
			mark_exits(3*height - 1, i);
		}
		for (size_t i = 0; i < 3 * height; i++)
		{
			mark_exits(i, 0);
			mark_exits(i, 3*width - 1);
		}
	}

	void mark_exits(int l, int c)
	{
		if(l < 0 || l >= 3 * height)
			return; /* Line out of bounds */
		if(c < 0 || c >= 3 * width)
			return; /* Column out of bounds */
		if(extended_data[l * 3 * width + c] != '.')
			return;
		
		extended_data[l * 3 * width + c] = 'O';
		mark_exits(l-1, c);
		mark_exits(l+1, c);
		mark_exits(l, c-1);
		mark_exits(l, c+1);
	}

	void mark_openings()
	{
		/* Translating the marking to the initial map */
		for(size_t line = 0; line < height; line++)
		{
			for (size_t col = 0; col < width; col++)
			{
				if(weighted_data[line * width + col] < 0)
				{
					if(extended_data[3*line * 3*width + 3*col] == 'O')
						data[line * width + col] = 'O';
					else
						data[line * width + col] = 'I';
				}
			}
		}
	}

	/**
	 * @brief Recursive map traveling, to update distances from the start
	 * 
	 * @param last_index[in] The last distance we were at
	 * @param l[in] The line to check
	 * @param c[in] The column to check
	 */
	void update_map(int last_index, size_t l, size_t c)
	{
		if(l < 0 || l >= height)
			return; /* Line out of bounds */
		if(c < 0 || c >= width)
			return; /* Column out of bounds */
		if(weighted_data[l * width + c] >= 0 && weighted_data[l * width + c] < last_index)
			return; /* Tile already parsed, with a shorter path */
		
		/* No need to check loop continuity, we suppose it's OK */
		switch (data[l * width + c])
		{
		case 'S':
			weighted_data[l * width + c] = last_index;
			/* Need to check if elements have the right orientation */
			if(data[(l+1) * width + (c)] == '|' || data[(l+1) * width + (c)] == 'L' || data[(l+1) * width + (c)] == 'J')
				update_map(last_index + 1, l+1, c);
			if(data[(l-1) * width + (c)] == '|' || data[(l-1) * width + (c)] == '7' || data[(l-1) * width + (c)] == 'F')
				update_map(last_index + 1, l-1, c);
			if(data[(l) * width + (c+1)] == '-' || data[(l) * width + (c+1)] == 'J' || data[(l) * width + (c+1)] == '7')
				update_map(last_index + 1, l, c+1);
			if(data[(l) * width + (c-1)] == '-' || data[(l) * width + (c-1)] == 'L' || data[(l) * width + (c-1)] == 'F')
				update_map(last_index + 1, l, c-1);
			break;

		case '|':
			weighted_data[l * width + c] = last_index;
			update_map(last_index + 1, l-1, c);
			update_map(last_index + 1, l+1, c);
			break;

		case '-':
			weighted_data[l * width + c] = last_index;
			update_map(last_index + 1, l, c-1);
			update_map(last_index + 1, l, c+1);
			break;

		case 'L':
			weighted_data[l * width + c] = last_index;
			update_map(last_index + 1, l-1, c);
			update_map(last_index + 1, l, c+1);
			break;

		case 'J':
			weighted_data[l * width + c] = last_index;
			update_map(last_index + 1, l-1, c);
			update_map(last_index + 1, l, c-1);
			break;

		case '7':
			weighted_data[l * width + c] = last_index;
			update_map(last_index + 1, l+1, c);
			update_map(last_index + 1, l, c-1);
			break;

		case 'F':
			weighted_data[l * width + c] = last_index;
			update_map(last_index + 1, l+1, c);
			update_map(last_index + 1, l, c+1);
			break;
		
		default:
			return;
		}
	}

	~pipe_map()
	{
		delete[] data;
		delete[] weighted_data;
		delete[] extended_data;
	}



};

std::ostream& operator<<(std::ostream& os, const struct pipe_map &p)
{
	for(size_t line = 0; line < p.height; line++)
	{
		for (size_t col = 0; col < p.width; col++)
		{
			os << p.data[line * p.width + col];
		}

		os << endl;
	}
	// os << endl << endl;
	// for(size_t line = 0; line < p.height; line++)
	// {
	// 	for (size_t col = 0; col < p.width; col++)
	// 	{
	// 		os << (char)('1' + p.weighted_data[line * p.width + col]);
	// 	}

	// 	os << endl;
	// }
	// for(size_t line = 0; line < p.height * 3; line++)
	// {
	// 	for (size_t col = 0; col < p.width * 3; col++)
	// 	{
	// 		os << p.extended_data[line * 3 * p.width + col];
	// 	}

	// 	os << endl;
	// }
	return os; 
}

// #define STAR_1


int main(int argc, char *argv[])
{
	size_t lines = 0, cols = 0;
	int enclosed_tiles = 0;
	string buffer;

	std::ifstream infile(argv[1]);

	do
	{
		getline(infile, buffer);
		cols = std::max(cols, buffer.length());
		lines++;
	} while (!infile.eof());
	infile.clear();
	infile.seekg(0, ios::beg);

	struct pipe_map pipe_map(cols, lines, infile);

	cout << "Furthest point is : " 
			<< *std::max_element(pipe_map.weighted_data, 
								pipe_map.weighted_data + (pipe_map.height * pipe_map.width))
			<< endl;

	for (size_t i = 0; i < pipe_map.height * pipe_map.width; i++)
	{
		if(pipe_map.data[i] == 'I')
			enclosed_tiles++;
	}
	
	cout << "Enclosed tiles : " << enclosed_tiles << endl;

	return 0;
}

