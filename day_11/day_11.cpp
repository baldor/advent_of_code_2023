#include <string>

#include <iostream>
#include <fstream>

#include <vector>
#include <algorithm>

using namespace std;


// #define STAR_1

int main(int argc, char *argv[])
{
	size_t line = 0;
	int max_col = 0;
	string buffer;

#ifdef STAR_1
	int growth = 1;
#else
	int growth = 1000000-1;/* each empty space is now 1'000'000 spaces */
#endif //STAR_1

	long long total_dist = 0;

	vector<std::pair<int, int>> galaxies;

	std::ifstream infile(argv[1]);

	do
	{
		getline(infile, buffer);
		for(int c = 0; c < buffer.length(); c++)
		{
			if(buffer.at(c) == '#')
			{
				galaxies.push_back(pair<int, int>(line, c));
				max_col = std::max(c, max_col);
			}
		}
		line++;
	} while (!infile.eof());

	/* Space line growth */
	for(int i = line - 1; i >= 0; i--)
	{
		bool found = false;
		for(const auto &g : galaxies)
		{
			if(g.first == i)
			{
				found = true;
				break;
			}
		}

		if(!found) /* Empty line of space */
		{
			for(auto &g : galaxies)
			{
				if(g.first > i)
					g.first += growth;
			}
		}
	}

	/* Space col growth*/
	for(int i = max_col - 1; i >= 0; i--)
	{
		bool found = false;
		for(const auto &g : galaxies)
		{
			if(g.second == i)
			{
				found = true;
				break;
			}
		}

		if(!found) /* Empty line of space */
		{
			for(auto &g : galaxies)
			{
				if(g.second > i)
					g.second+= growth;
			}
		}
	}

	/* Compute distances */
	for(const auto &g1 : galaxies)
	{
		for(const auto &g2 : galaxies)
		{
			if(g1 == g2)
			{
				continue;
			}
			total_dist += abs(g2.first-g1.first) + abs(g1.second-g2.second);
		}
	}

	total_dist /= 2; /* Every distance was counted twice */
	cout << "Sum of all distances : " << total_dist << endl;

	return 0;
}

