#include <string>

#include <iostream>
#include <iterator>
#include <fstream>
#include <sstream>

#include <map>

#include <algorithm>


using namespace std;

enum HANDS {
	HIGH = 0,
	PAIR,
	TWO_PAIRS,
	THREE,
	FULL,
	FOUR,
	FIVE
};

struct hand
{
	string initval;
	string val;
	enum HANDS hand_type;

	hand(string value) : initval(value), val(value), hand_type(HANDS::HIGH)
	{
		std::replace(val.begin(), val.end(), 'A', 'E');
		std::replace(val.begin(), val.end(), 'K', 'D');
		std::replace(val.begin(), val.end(), 'Q', 'C');
		std::replace(val.begin(), val.end(), 'T', 'A');

#ifdef STAR_1
		std::replace(val.begin(), val.end(), 'J', 'B');
#else
		std::replace(val.begin(), val.end(), 'J', '1');
#endif // STAR_1
	
		map<char, int> counting_cards;
		using pair_type = decltype(counting_cards)::value_type;
		for(const char& c : val)
		{
			if(counting_cards.find(c) != counting_cards.end())
				counting_cards[c]++;
			else
				counting_cards[c] = 1;
		}

		if(counting_cards.find('1') != counting_cards.end()) /* A joker is found */
		{
			auto m = std::max_element(counting_cards.begin(), counting_cards.end(), [](const pair_type & p1, const pair_type & p2){
				return p1.first == '1' || p1.second < p2.second;
			});
			if(m->first != '1')
			{
				m->second += counting_cards['1'];
				counting_cards['1'] = 0;
			}
		}

		for(const auto &[k,v] : counting_cards)
		{
			switch (v)
			{
			case 1: break; /* High card, can't go lower than that */
			case 2: hand_type = (hand_type == PAIR) ? TWO_PAIRS : ((hand_type == THREE) ? FULL : PAIR); break;
			case 3: hand_type = (hand_type == PAIR) ? FULL : THREE; break;
			case 4: hand_type = HANDS::FOUR; break;
			case 5: hand_type = HANDS::FIVE; break;
			default: break;
			}
		}
	}

	bool operator<(const struct hand &rhs) const
	{
		if(hand_type == rhs.hand_type)
			return val < rhs.val;
		
		return hand_type < rhs.hand_type;
	}
};


// #define STAR_1


int main(int argc, char *argv[])
{
		
	std::ifstream infile(argv[1]);
	map<struct hand, long> hand_rating;

	do
	{
		int bid;
		string cards;
		infile >> cards >> bid;

		struct hand hand(cards);
		hand_rating[hand] = bid;

	} while (!infile.eof());

	long final_bid = 0;
	int i = 1;
	for(const auto &[k,v] : hand_rating)
	{
		final_bid += v * i;
		i++;
	}

	cout << "Total bid won : " << final_bid << endl;

	return 0;
}

