#include <string>

#include <iostream>
#include <fstream>
#include <sstream>

#include <vector>
#include <map>
#include <numeric>

using namespace std;

// #define STAR_1

// #ifdef STAR_1
// #else
// #endif // STAR_1

int main(int argc, char *argv[])
{
	int curID = 0;
	int sum_of_cards = 0;
	string buffer;
		
	std::ifstream infile(argv[1]);
	vector<int> cards;

	/* Parsing input */
	while (!infile.eof())
	{
		string numList;

		/* Line by line */
		getline(infile, buffer);

		if(buffer.length() == 0)
			continue; /* Not a line to parse */
		
		stringstream ss(buffer);

		if(cards.size() <= curID)
			cards.push_back(1); /* Add a new card to our stockpile */
		else
			cards.at(curID) ++;

		getline(ss, numList, ':'); /* Get the card ID out */

		map<int, int> winning_num;
		int total_numbers = 0;
		bool parse_winning = true;

		/* Now parse the winning numbers and played numbers */
		while(ss.tellg() >= 0)
		{
			string elem;

			getline(ss, elem, '|'); /* Get the next draw */
			stringstream ss2(elem);

			/* Now parse the elements of a draw*/
			while(ss2.tellg() >= 0)
			{
				int value;
				ss2 >> value;
				if(parse_winning)
					winning_num[value] = value;
				else
				{
					if(winning_num.find(value) != winning_num.end())
						total_numbers++;
				}
			}

			parse_winning = false;

		}

		for(int i = 0; i < total_numbers; i++)
		{
			if(cards.size() <= curID + i + 1)
				cards.push_back(cards.at(curID));
			else
				cards.at(curID + i + 1) += cards.at(curID);
		}

		sum_of_cards += (1 << (total_numbers-1));
		curID++;
	}

	int total_cards = std::accumulate(cards.begin(), cards.end(), 0);

	cout << "Total : " << sum_of_cards << endl;
	cout << "Total cards : " << total_cards << endl;

	return 0;
}

