#include <string>

#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

// #define STAR_1

int main(int argc, char *argv[])
{
	int curID = 1;
	string buffer;

#ifdef STAR_1
	int total = 0;

	int limit_red = 12;
	int limit_green = 13;
	int limit_blue = 14;
#else
	int power_total = 0;
#endif // STAR_1
		
	std::ifstream infile(argv[1]);

	/* Parsing input */
	while (!infile.eof())
	{
		bool ok = true; /* If the game is possible */
		string draw;

#ifndef STAR_1
		int min_red = 0;
		int min_green = 0;
		int min_blue = 0;
#endif // STAR_1

		/* Line by line */
		getline(infile, buffer);

		if(buffer.length() == 0)
			continue; /* Not a line to parse */
		
		stringstream ss(buffer);

		getline(ss, draw, ':'); /* Get the game ID out */

		/* Now parse the draws in a game */
		while(ss.tellg() >= 0)
		{
			string elem;

			getline(ss, draw, ';'); /* Get the next draw */
			stringstream ss2(draw);

			/* Now parse the elements of a draw*/
			while(ss2.tellg() >= 0)
			{
				getline(ss2, elem, ','); /* Get the next element */
				stringstream ss3(elem);

				int value;
				string color;
				ss3 >> value >> color;

				if(color == "red")
				{
#ifdef STAR_1
					if(value > limit_red)
					{
						ok = false;
						break; /* Get out of the loop and go to the next line */
					}
#else
					min_red = std::max(value, min_red);
#endif // STAR_1
				}
				else if(color == "blue")
				{
#ifdef STAR_1
					if(value > limit_blue)
					{
						ok = false;
						break; /* Get out of the loop and go to the next line */
					}
#else
					min_blue = std::max(value, min_blue);
#endif // STAR_1
				}
				else if(color == "green")
				{
#ifdef STAR_1
					if(value > limit_green)
					{
						ok = false;
						break; /* Get out of the loop and go to the next line */
					}
#else
					min_green = std::max(value, min_green);
#endif // STAR_1
				}
				else
				{
					cerr << "ERROR !!!! How did you get here?" << endl;
				}

			}

			if(!ok)
				break; /* Get out of the loop and go to the next line */
		}
#ifdef STAR_1
		if(ok)
			total += curID;
#else
		int power = min_red * min_blue * min_green;
		power_total += power;

#endif // STAR_1

		
		curID++;
	}

#ifdef STAR_1
	cout << "Sum of possible games : " << total << endl;
#else
	cout << "Sum of power games : " << power_total << endl;
#endif // STAR_1

	return 0;
}

