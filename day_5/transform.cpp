#include "day_5.h"

long translate_value(long value, std::vector<struct map_element> &list)
{
	for(const auto &el : list)
	{
		if(value < el.source) /* Didn't match any past element, and not the current one -> mapped to itself */
		{
			return value;
		}
		if(value < el.source + el.len) /* Matches current element, adding offset to map */
		{

			return value + el.offset;
		}
	}
	return value; /* Didn't match anything, mapping to itself */
}


void translate_ranges(const struct seed_range &value, std::vector<struct seed_range> &transformed, std::vector<struct map_element> &list)
{
	long start = value.start;

	
	for(const auto &el : list)
	{
		if(value.end < el.source) /* We finished to parse the source range */
		{
			struct seed_range tr;
			tr.start = start;
			tr.end = value.end;
			transformed.push_back(tr);
			return;
		}
		else if(start < el.source) /* Didn't match any past element, and not the current one -> range mapped to itself */
		{
			struct seed_range tr;
			tr.start = start;
			tr.end = el.source - 1;
			transformed.push_back(tr);

			start = el.source;
			
			/* But wait, there's more! we must continue with the current element */
			if(value.end <= start)
				return;
			if(value.end <= el.source + el.len)/* The rest is entirely inside the current range */
			{
				struct seed_range tr_in;
				tr_in.start = start + el.offset;
				tr_in.end = value.end + el.offset;
				transformed.push_back(tr_in);
				return;
			}
			else /* We will still continue */
			{
				struct seed_range tr_in;
				tr_in.start = start + el.offset;
				tr_in.end = el.source + el.len + el.offset;
				transformed.push_back(tr_in);

				start = el.source + el.len + 1; /* End value + 1 */
			}
		}
		else if(start >= el.source && start < el.source + el.len) /* We're inside the range */
		{
			if(value.end > el.source + el.len) /* Our range is bigger than the target range, creating a sub range first */
			{
				struct seed_range tr_target;

				tr_target.start = start + el.offset;
				tr_target.end = el.source + el.len + el.offset;
				transformed.push_back(tr_target);

				start = el.source + el.len + 1; /* End value + 1 */
			}
			else if(start < value.end) /* Our range is entirely inside the target range */
			{
				struct seed_range tr_target;

				tr_target.start = start + el.offset;
				tr_target.end = value.end + el.offset;
				transformed.push_back(tr_target);

				return;
			}
		}

		if(start > value.end)
			return;
	}
	/* Didn't match anything, mapping to itself */
	struct seed_range tr;
	tr.start = start;
	tr.end = value.end;
	transformed.push_back(tr);

	return; 
}
