#ifndef DAY_5_H_
#define DAY_5_H_

#include <vector>
#include <string>

struct map_element {
	long source;
	long len;
	long offset;
};

struct seed_range {
	long start;
	long end;
};

void decode_to_list(std::string &input, std::vector<long> &list);
void decode_to_ranges(std::string &input, std::vector<struct seed_range> &list);
void decode_to_map(std::string &input, std::vector<struct map_element> &list);

bool sort_map(struct map_element &i, struct map_element &j);
bool sort_range(struct seed_range &i, struct seed_range &j);
long translate_value(long value, std::vector<struct map_element> &list);
void translate_ranges(const struct seed_range &value, std::vector<struct seed_range> &transformed, std::vector<struct map_element> &list);

int main_seeds(char *input);
int main_ranges(char *input);

#endif //DAY_5_H_