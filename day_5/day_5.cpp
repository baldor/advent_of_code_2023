#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <map>
#include <cstring>

#include "day_5.h"


using namespace std;


enum state {
	NONE,
	SEEDS,
	SEED2SOIL,
	SOIL2FERT,
	FERT2WATER,
	WATER2LIGHT,
	LIGHT2TEMP,
	TEMP2HUM,
	HUM2LOC
};

// vector<int> seeds;
// vector<struct map_element> seed_to_soil;

int main(int argc, char *argv[])
{
	if(argc <= 2 && strcmp(argv[1], "--help") == 0)
	{
		cout << "Usage : " << endl;
		cout << argv[0] << " <filename> \t\t Single seeds" << endl;
		cout << argv[0] << " <filename> ranges \t Ranged seeds" << endl;
	}
	else if(argc > 2 && argv[2][0] == 'r')
		main_ranges(argv[1]);
	else
		main_seeds(argv[1]);
	
	return 0;
}


int main_seeds(char *input)
{
	/* Needed structures */
	vector<long> seeds;
	vector<struct map_element> seed_to_soil;
	vector<struct map_element> soil_to_fert;
	vector<struct map_element> fert_to_water;
	vector<struct map_element> water_to_light;
	vector<struct map_element> light_to_temp;
	vector<struct map_element> temp_to_hum;
	vector<struct map_element> hum_to_loc;

	map<long, long> loc_to_seed;

	
	/* Parsing input */
	state status = NONE;
	string buffer, str;
	
	std::ifstream infile(input);

	getline(infile, buffer);

	do
	{
		if(buffer.find("seeds") != std::string::npos)
		{
			str = buffer.substr(buffer.find(":")+2);
			status = SEEDS;
		}
		else if(buffer.find("seed-to-soil") != std::string::npos)
		{
			str = buffer.substr(buffer.find(":")+1);
			status = SEED2SOIL;
		}
		else if(buffer.find("soil-to-fertilizer") != std::string::npos)
		{
			str = buffer.substr(buffer.find(":")+1);
			status = SOIL2FERT;
		}
		else if(buffer.find("fertilizer-to-water") != std::string::npos)
		{
			str = buffer.substr(buffer.find(":")+1);
			status = FERT2WATER;
		}
		else if(buffer.find("water-to-light") != std::string::npos)
		{
			str = buffer.substr(buffer.find(":")+1);
			status = WATER2LIGHT;
		}
		else if(buffer.find("light-to-temperature") != std::string::npos)
		{
			str = buffer.substr(buffer.find(":")+1);
			status = LIGHT2TEMP;
		}
		else if(buffer.find("temperature-to-humidity") != std::string::npos)
		{
			str = buffer.substr(buffer.find(":")+1);
			status = TEMP2HUM;
		}
		else if(buffer.find("humidity-to-location") != std::string::npos)
		{
			str = buffer.substr(buffer.find(":")+1);
			status = HUM2LOC;
		}

		switch (status)
		{
		case SEEDS:
			decode_to_list(str, seeds);
			break;
		case SEED2SOIL:
			decode_to_map(str, seed_to_soil);
			break;	
		case SOIL2FERT:
			decode_to_map(str, soil_to_fert);
			break;
		case FERT2WATER:
			decode_to_map(str, fert_to_water);
			break;
		case WATER2LIGHT:
			decode_to_map(str, water_to_light);
			break;
		case LIGHT2TEMP:
			decode_to_map(str, light_to_temp);
			break;
		case TEMP2HUM:
			decode_to_map(str, temp_to_hum);
			break;
		case HUM2LOC:
			decode_to_map(str, hum_to_loc);
			break;
			
		default:
			break;
		}

		getline(infile, buffer);
		str = buffer;
	} while (!infile.eof());


	/* Sorting maps */
	std::sort(seed_to_soil.begin(), seed_to_soil.end(), sort_map);
	std::sort(soil_to_fert.begin(), soil_to_fert.end(), sort_map);
	std::sort(fert_to_water.begin(), fert_to_water.end(), sort_map);
	std::sort(water_to_light.begin(), water_to_light.end(), sort_map);
	std::sort(light_to_temp.begin(), light_to_temp.end(), sort_map);
	std::sort(temp_to_hum.begin(), temp_to_hum.end(), sort_map);
	std::sort(hum_to_loc.begin(), hum_to_loc.end(), sort_map);


	/* Transforming seeds */
	for(auto seed : seeds)
	{
		long res = translate_value(seed, seed_to_soil);
		res = translate_value(res, soil_to_fert);
		res = translate_value(res, fert_to_water);
		res = translate_value(res, water_to_light);
		res = translate_value(res, light_to_temp);
		res = translate_value(res, temp_to_hum);
		res = translate_value(res, hum_to_loc);

		loc_to_seed[res] = seed; /* Overkill, just need to keep the min value */
	}


	cout << "Answer : " << loc_to_seed.begin()->first << endl;



	return 0;
}


int main_ranges(char *input)
{
	/* Needed structures */
	vector<struct seed_range> seeds;

	vector<struct map_element> seed_to_soil;
	vector<struct map_element> soil_to_fert;
	vector<struct map_element> fert_to_water;
	vector<struct map_element> water_to_light;
	vector<struct map_element> light_to_temp;
	vector<struct map_element> temp_to_hum;
	vector<struct map_element> hum_to_loc;

	
	/* Parsing input */
	state status = NONE;
	string buffer, str;
	
	std::ifstream infile(input);

	getline(infile, buffer);

	do
	{
		if(buffer.find("seeds") != std::string::npos)
		{
			str = buffer.substr(buffer.find(":")+2);
			status = SEEDS;
		}
		else if(buffer.find("seed-to-soil") != std::string::npos)
		{
			str = buffer.substr(buffer.find(":")+1);
			status = SEED2SOIL;
		}
		else if(buffer.find("soil-to-fertilizer") != std::string::npos)
		{
			str = buffer.substr(buffer.find(":")+1);
			status = SOIL2FERT;
		}
		else if(buffer.find("fertilizer-to-water") != std::string::npos)
		{
			str = buffer.substr(buffer.find(":")+1);
			status = FERT2WATER;
		}
		else if(buffer.find("water-to-light") != std::string::npos)
		{
			str = buffer.substr(buffer.find(":")+1);
			status = WATER2LIGHT;
		}
		else if(buffer.find("light-to-temperature") != std::string::npos)
		{
			str = buffer.substr(buffer.find(":")+1);
			status = LIGHT2TEMP;
		}
		else if(buffer.find("temperature-to-humidity") != std::string::npos)
		{
			str = buffer.substr(buffer.find(":")+1);
			status = TEMP2HUM;
		}
		else if(buffer.find("humidity-to-location") != std::string::npos)
		{
			str = buffer.substr(buffer.find(":")+1);
			status = HUM2LOC;
		}

		switch (status)
		{
		case SEEDS:
			decode_to_ranges(str, seeds);
			break;
		case SEED2SOIL:
			decode_to_map(str, seed_to_soil);
			break;	
		case SOIL2FERT:
			decode_to_map(str, soil_to_fert);
			break;
		case FERT2WATER:
			decode_to_map(str, fert_to_water);
			break;
		case WATER2LIGHT:
			decode_to_map(str, water_to_light);
			break;
		case LIGHT2TEMP:
			decode_to_map(str, light_to_temp);
			break;
		case TEMP2HUM:
			decode_to_map(str, temp_to_hum);
			break;
		case HUM2LOC:
			decode_to_map(str, hum_to_loc);
			break;
			
		default:
			break;
		}

		getline(infile, buffer);
		str = buffer;
	} while (!infile.eof());


	/* Sorting maps */
	std::sort(seed_to_soil.begin(), seed_to_soil.end(), sort_map);
	std::sort(soil_to_fert.begin(), soil_to_fert.end(), sort_map);
	std::sort(fert_to_water.begin(), fert_to_water.end(), sort_map);
	std::sort(water_to_light.begin(), water_to_light.end(), sort_map);
	std::sort(light_to_temp.begin(), light_to_temp.end(), sort_map);
	std::sort(temp_to_hum.begin(), temp_to_hum.end(), sort_map);
	std::sort(hum_to_loc.begin(), hum_to_loc.end(), sort_map);


	/* Transforming seeds */
	vector<struct seed_range> soils_ranges;
	for(const auto &seed : seeds)
		translate_ranges(seed, soils_ranges, seed_to_soil);

	vector<struct seed_range> fert_ranges;
	for(const auto &soil : soils_ranges)
		translate_ranges(soil, fert_ranges, soil_to_fert);
		
	vector<struct seed_range> water_ranges;
	for(const auto &fert : fert_ranges)
		translate_ranges(fert, water_ranges, fert_to_water);
		
	vector<struct seed_range> light_ranges;
	for(const auto &water : water_ranges)
		translate_ranges(water, light_ranges, water_to_light);
		
	vector<struct seed_range> temp_ranges;
	for(const auto &light : light_ranges)
		translate_ranges(light, temp_ranges, light_to_temp);
		
	vector<struct seed_range> hum_ranges;
	for(const auto &temp : temp_ranges)
		translate_ranges(temp, hum_ranges, temp_to_hum);
		
	vector<struct seed_range> loc_ranges;
	for(const auto &hum : hum_ranges)
		translate_ranges(hum, loc_ranges, hum_to_loc);

	std::sort(loc_ranges.begin(), loc_ranges.end(), sort_range);

	cout << "Answer : " << loc_ranges[0].start << endl;



	return 0;
}



bool sort_map(struct map_element &i, struct map_element &j)
{
	return i.source < j.source;
}

bool sort_range(struct seed_range &i, struct seed_range &j)
{
	return i.start < j.start;
}
