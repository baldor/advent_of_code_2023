#include <sstream>

#include "day_5.h"

using namespace std;

/**
 * @brief Decode a line of input to a vector of long
 * 
 * @param input[in] The input line
 * @param list[out] The decoded list
 */
void decode_to_list(std::string &input, std::vector<long> &list)
{
	stringstream ss;
	ss << input;

	long val = 0;
	while(ss.tellg() >= 0)
	{
		string buf;
		ss >> buf;

		if(buf.length() > 0)
		{
			val = stol(buf);
			list.push_back(val);
		}
	}
}

/**
 * @brief Decode an input line to a list of ranges
 * 
 * @param input[in] The input line
 * @param list[out] A list of ranges (start - end)
 */
void decode_to_ranges(std::string &input, std::vector<struct seed_range> &list)
{
	stringstream ss;
	ss << input;

	while(ss.tellg() >= 0)
	{
		string buf1, buf2;
		ss >> buf1 >> buf2;

		if(buf1.length() > 0 && buf2.length() > 0)
		{
			struct seed_range el;
			el.start = stol(buf1);
			el.end = el.start + stol(buf2) - 1;

			list.push_back(el);
		}
	}
}

/**
 * @brief Decode an input line to a list of mapping structure ([source, source+len] -> [target, target+len] )
 * 
 * @param input[in] The input line
 * @param list[out] A list of mapping elements
 */
void decode_to_map(std::string &input, std::vector<struct map_element> &list)
{
	stringstream ss;
	ss << input;

	long val1 = 0;
	long val2 = 0;
	long val3 = 0;
	while(ss.tellg() >= 0)
	{
		string buf1, buf2, buf3;
		ss >> buf1 >> buf2 >> buf3;

		if(buf1.length() > 0 && buf2.length() > 0 && buf3.length() > 0)
		{
			struct map_element el;
			val1 = stol(buf1);
			val2 = stol(buf2);
			val3 = stol(buf3);

			el.source = val2;
			el.len = val3-1;
			el.offset = val1 - val2;
			list.push_back(el);
		}
	}
}
