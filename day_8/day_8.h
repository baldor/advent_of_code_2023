#ifndef DAY_8_H
#define DAY_8_H

#include <string>

struct LR_Input
{
	std::string left;
	std::string right;
};

struct Gost
{
	std::string ghost;
	std::string final;
	int nb_moves;
};


#endif /* DAY_8_H */
