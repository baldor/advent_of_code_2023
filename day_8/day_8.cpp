#include <string>

#include <iostream>
#include <fstream>
#include <sstream>

#include <vector>

#include <algorithm>
#include <map>
#include <iterator>
#include <numeric>

#include "day_8.h"

using namespace std;

/* Uncomment to test cycle length (from ??Z back to ??Z) */
//#define CYCLE_TEST

bool finished(const std::vector<std::string> &ghosts);

int main(int argc, char *argv[])
{
	string buffer;
	string instructions;

	map<string, struct LR_Input> path;
		
	std::ifstream infile(argv[1]);

	/* First line is instructions */
	getline(infile, instructions);

	/* Second line is thrown away*/
	getline(infile, buffer);

	/* Parsing input */
	while (!infile.eof())
	{
		string result, elem;
		struct LR_Input values;
		getline(infile, buffer);

		std::replace(buffer.begin(), buffer.end(), '(', ' ');
		std::replace(buffer.begin(), buffer.end(), ')', ' ');
		std::replace(buffer.begin(), buffer.end(), '=', ',');
		std::remove_copy(buffer.begin(), buffer.end(), std::back_inserter(result), ' ');

		stringstream ss(result);

		getline(ss, elem, ',');
		getline(ss, values.left, ',');
		getline(ss, values.right, ',');

		if(elem.length() < 3)
			continue;
		path[elem] = values;
	}
	
	/* Now we solve! */
	vector<struct Gost> ghosts;
#ifdef CYCLE_TEST
	vector<struct Gost> cycles;
#endif

	/* We suppose there are cycles, and no cycle contains 2 Z */
	for( const auto& [key, value] : path)
	{
		if(key.at(2) != 'A')
			continue;
		struct Gost g;

		g.ghost = key;
		g.nb_moves = 0;
		ghosts.push_back(g);

	}

#ifdef CYCLE_TEST
	/* We suppose there are cycles, and no cycle contains 2 */
	for( const auto& [key, value] : path)
	{
		if(key.at(2) != 'Z')
			continue;
		struct Gost g;

		g.ghost = key;
		g.nb_moves = 0;
		cycles.push_back(g);

	}
#endif
	
	for(auto &ghost : ghosts)
	{
		string moving = ghost.ghost;
		cout << "Parsing for " << moving << endl;
		while (moving.at(2) != 'Z')
		{
			if(instructions.at(ghost.nb_moves % instructions.length()) == 'L')
				moving = path[moving].left;
			else
				moving = path[moving].right;
			ghost.nb_moves++;
		}

		ghost.final = moving;

		cout << "Got from " << ghost.ghost << " to " << ghost.final << " in " << ghost.nb_moves << endl;
		
	}

#ifdef CYCLE_TEST
	for(auto &ghost : cycles)
	{
		// nb_moves = 0;
		string moving = ghost.ghost;
		cout << "Parsing for " << moving << endl;
		do
		{
			if(instructions.at(ghost.nb_moves % instructions.length()) == 'L')
				moving = path[moving].left;
			else
				moving = path[moving].right;
			ghost.nb_moves++;
		} while (moving.at(2) != 'Z');

		ghost.final = moving;

		cout << "Got from " << ghost.ghost << " to " << ghost.final << " in " << ghost.nb_moves << endl;
		
	}
#endif

	/* Cycles are all equal (time to arrive to a Z, and time to make a loop), so now, we only need to find the lcm of all */
	long lcm = 0;
	
	for(auto &ghost : ghosts)
	{
		if(lcm == 0)
			lcm = ghost.nb_moves;
		else
		{
			int gcd = std::gcd(lcm, ghost.nb_moves);
			lcm = lcm / gcd * ghost.nb_moves;
		}
	}

	cout << "Took at most " << lcm << " moves." << endl;
	


	return 0;
}

bool finished(const std::vector<std::string> &ghosts)
{
	for(const auto &ghost : ghosts)
	{
		if(ghost.at(2) != 'Z')
			return false;
	}

	return true;
}
