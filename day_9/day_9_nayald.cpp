#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <iterator>
#include <chrono>

int nayald_main(char *argv, bool show_result) {
        std::ifstream file(argv);
        if (!file.is_open()) {
                std::cerr << "file is not open" << std::endl;
                return -1;
        }

int sum_right = 0;
        int sum_left = 0;
	std::vector<int> ediff;
        std::string line;
        while (std::getline(file, line)) {
                // parse file line
                std::istringstream iss(line);
                std::copy(std::istream_iterator<int>(iss), std::istream_iterator<int>(), std::back_inserter(ediff));

		// right is just the sum of rightmost elements
		sum_right += ediff.back();
		// left can be a sum of alternated plus/minus leftmost elements, example with 4 "layers": y = a - (b - (c - d)), can be expressed as y = a - b + c - d
		sum_left += ediff.front();
		int coef = -1;		

                // compute adjacent diff
                int zero;
                do {
                        zero = 0;
                        for (int i = 0; i < ediff.size() - 1; ++i) {
                                ediff[i] = ediff[i + 1] - ediff[i];
                                zero |= ediff[i];
                        }

			ediff.pop_back();
			sum_right += ediff.back();
			sum_left += coef * ediff.front();
			coef *= -1;
                } while (zero != 0);

		ediff.clear();
        }


        if(show_result)
			std::cout << "part 1 answer => " << sum_right << ", part 2 answer => " << sum_left << std::endl;
        return 0;
}