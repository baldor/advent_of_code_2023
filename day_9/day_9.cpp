#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <chrono>

#include <thread>

using namespace std;

void binomial_signed_coef_list(int len, struct binomial_coeffs *coeff_list);

struct binomial_coeffs
{
	long long *coeffs;

	binomial_coeffs(int size) : coeffs(new long long[size]){}
	~binomial_coeffs(){
		delete[] coeffs;
	}
};


int baldor_main(char *argv, bool show_result)
{
	map<int, struct binomial_coeffs *> coefs;
	
	long num;
	long long resultForward = 0;
	long long resultBackward = 0;
		
	std::ifstream infile(argv);

	do
	{
		vector<long long> values;
		values.reserve(21); /* Optimized for 21 elements or less */
		
		/* Decode all numbers */
		while (infile.peek() != '\n' && !infile.eof())
		{
			infile >> num;
			values.push_back(num);
		}

		int len = values.size();

		if(len == 0)
			break;

		infile.get();
		
		struct binomial_coeffs *arr;
	
		auto elem = coefs.find(len);
		if(elem == coefs.end())
		{ /* Store the binomial coefficients */
			arr = new struct binomial_coeffs(len + 1);		
			binomial_signed_coef_list(len, arr);
			coefs[len] = arr;
		}
		else
		{ /* if it exists, get it*/
			arr = elem->second;
		}

		long long totalForward = 0;
		long long totalBackward = 0;
		for(int i = 0; i < len - 1; i++)
		{
			/* Compute line result forward */
			totalForward += values[i] * arr->coeffs[i];
			/* Compute line result backward */
			totalBackward += values[i] * arr->coeffs[i+1];
		}
		totalForward += values[len - 1] * arr->coeffs[len - 1];
		
		
		resultForward += totalForward;
		if(len % 2)
			resultBackward += values[len - 1] - totalBackward;
		else
			resultBackward += totalBackward - values[len - 1];

	}
	while (!infile.eof());
	
	if(show_result)
	{
		std::cout << "Extrapolated forward : " << resultForward << endl;
		std::cout << "Extrapolated backward : " << resultBackward << endl;
	}

	return resultBackward;
}

/**
 * @brief Get the signed binomial coefficients
 * ex : len = 6, => 1 -6 15 -20 15 -6 1
 * ex : len = 5, => -1 5 -10 10 -5 1
 * 
 * @param len[in] The binomial length
 * @param pos[in] The wanted position
 * @return int The signed coefficient
 */
void binomial_signed_coef_list(int len, struct binomial_coeffs *coeff_list)
{
	coeff_list->coeffs[0] = 1;

	for (int k = 0; k < len; ++ k)
	{
    	coeff_list->coeffs[k + 1] = (coeff_list->coeffs[k] * (len - k)) / (k + 1);
	}

	for (int i = 0; i <= len; i++)
	{
		if(!((i + len) & 0x1)) /* Signing the coefficient */
			coeff_list->coeffs[i] = -coeff_list->coeffs[i];
	}
}