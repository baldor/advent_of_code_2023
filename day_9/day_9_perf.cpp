#include <chrono>
#include <iostream>

int baldor_main(char *argv, bool);
int nayald_main(char *argv, bool);

using namespace std;

int main(int argc, char *argv[])
{
	int count = atoi(argv[2]);
	std::chrono::microseconds total_baldor(0);
	std::chrono::microseconds fastest_baldor(INT64_MAX);
	std::chrono::microseconds slowest_baldor(0);
	std::chrono::microseconds total_nayald(0);
	std::chrono::microseconds fastest_nayald(INT64_MAX);
	std::chrono::microseconds slowest_nayald(0);

	for(int i = 0; i < count; i++)
	{
		const auto start = std::chrono::steady_clock::now();
		baldor_main(argv[1], true);
		auto duration = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - start);
		total_baldor += duration;
		fastest_baldor = std::min(fastest_baldor, duration);
		slowest_baldor = std::max(slowest_baldor, duration);

	}
	for(int i = 0; i < count; i++)
	{
		const auto start = std::chrono::steady_clock::now();
		nayald_main(argv[1], true);
		auto duration = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - start);
		total_nayald += duration;
		fastest_nayald = std::min(fastest_nayald, duration);
		slowest_nayald = std::max(slowest_nayald, duration);
	}

	cout << "Statistics for baldor : Min = " << fastest_baldor << ", Max = " << slowest_baldor << ", Mean = " << total_baldor / count << endl;
	cout << "Statistics for nayald : Min = " << fastest_nayald << ", Max = " << slowest_nayald << ", Mean = " << total_nayald / count << endl;

}