#include <string>

#include <iostream>
#include <fstream>
#include <sstream>

#include <vector>
#include <map>

using namespace std;

bool get_next_num(const vector<string> &schematic, int &line, int &column);
int parse_num(const vector<string> &schematic, int line, int column);
bool is_num_part(const vector<string> &schematic, int line, int column);
bool check_position(const vector<string> &schematic, int line, int column);
void register_gear_part(const vector<string> &schematic, int line, int column, int value, map<struct pos, vector<int>> &gears);


struct pos
{
	int line;
	int col;

	bool operator==(const struct pos& rhs) const
	{
		return this->col == rhs.col && this->line == rhs.line;
	}

	bool operator<(const struct pos& rhs) const
	{
		if(this->line == rhs.line)
			return this->col < rhs.col;
		
		return this->line < rhs.line;
	}
};


// #define STAR_1

int main(int argc, char *argv[])
{
	/* Starting values */
	int line = -1;
	int col = -1;

	long total = 0;

	string buffer;

	vector<string> schematic;
#ifndef STAR_1
	map<struct pos, vector<int>> gears;
#endif // STAR_1
		
	std::ifstream infile(argv[1]);

	/* Parsing input */
	while (!infile.eof())
	{
		/* Line by line */
		getline(infile, buffer);

		if(buffer.length() == 0)
			continue; /* Not a line to parse */
		
		schematic.push_back(buffer);
	}

	while(get_next_num(schematic, line, col))
	{
#ifdef STAR_1

		if(is_num_part(schematic, line, col))
			total += parse_num(schematic, line, col);
#else
		register_gear_part(schematic, line, col, parse_num(schematic, line, col), gears);
#endif // STAR_1
	}

#ifndef STAR_1
	for(const auto &[k, v] : gears)
	{
		if(v.size() == 2)
		{
			total += v[0] * v[1];
		}
	}
#endif // STAR_1

	cout << "Sum of parts : " << total << endl;

	return 0;
}

bool get_next_num(const vector<string> &schematic, int &line, int &column)
{
	bool return_on_next_number = false;

	if(column == -1)
	{
		return_on_next_number = true;
		column = 0;
	}

	if(line == -1)
	{
		return_on_next_number = true;
		line = 0;
	}

	for(uint i = line; i < schematic.size(); i++)
	{
		uint initCol = (i == line) ? column : 0; /* We don't want to start over from the start of the line each time */
		const string &curLine = schematic[i];
		for(uint j = initCol; j < curLine.length(); j++)
		{
			char c = curLine.at(j);
			if(c >= '0' && c <= '9')
			{
				if(return_on_next_number)
				{
					line = i;
					column = j;
					return true;
				}
				/* Else, we are still on the same number */
			}
			else
				return_on_next_number = true; /* We left current number, so next time we encounter a digit is a new number */
		}
		return_on_next_number = true; /* We changed lines, so next digit is a new number */
	}

	return false;
}

int parse_num(const vector<string> &schematic, int line, int column)
{
	int num = 0;
	const string &sline = schematic[line];

	for(uint i = column; i < sline.length(); i++)
	{
		char c = sline.at(i);
		if(c >= '0' && c <= '9')
		{
			num *= 10;
			num += c - '0';
		}
		else /* Not a digit, end of the number */
			return num;
	}

	return num;
}

bool check_position(const vector<string> &schematic, int line, int column)
{
	/* Edge cases */

	if(line < 0 || column < 0)
		return false;
	if(line >= schematic.size())
		return false;

	const string &sline = schematic[line];
	if(column >= sline.length())
		return false;

	char c = sline.at(column);

	if((c >= '0' && c <= '9') || c == '.')
		return false; /* Not an interesting char */

	return true;
}

bool is_num_part(const vector<string> &schematic, int line, int column)
{
	bool result = false;
	const string &sline = schematic[line];

	for(uint i = column; i < sline.length(); i++)
	{
		char c = sline.at(i);
		if(c >= '0' && c <= '9')
		{
			for(int j = -1; j <= 1; j++)
			{
				for(int k = -1; k <= 1; k++)
				{
					result |= check_position(schematic, line +j, i + k);
					if(result)
						return result; /* Already true, no need to continue */
				}
			}
		}
		else /* Not a digit, end of the number, so return */
			return result;
	}

	return result;
}

/**
 * @brief Register a gear part into a map, if it's related to a * gear
 * 
 * @param schematic[in] The schematic
 * @param line[in] The part number line
 * @param column[in] The part number column
 * @param value[in] The part number
 * @param gears[out] The map of all gears
 * @return true 
 * @return false 
 */
void register_gear_part(const vector<string> &schematic, int line, int column, int value, map<struct pos, vector<int>> &gears)
{
	bool first = true;
	bool result = false;
	const string &sline = schematic[line];

	for(uint i = column; i < sline.length(); i++)
	{
		char c = sline.at(i);
		if(c >= '0' && c <= '9')
		{
			for(int k = (first ? -1 : 1); k <= 1; k++) /* we don't want to loop multiple times on the same gears */
			{
				for(int j = -1; j <= 1; j++)
				{
					result = check_position(schematic, line + j, i + k);
					if(result)
					{
						if(schematic[line + j].at(i + k) == '*') /* Only star gears */
						{
							struct pos p = {.line = line + j, .col = i+k};
							gears[p].push_back(value);
						}
					}
				}

				first = false;
			}
		}
		else /* Not a digit, end of the number, so return */
			return;
	}
	return;
}