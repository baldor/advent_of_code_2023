#include <string>

#include <iostream>
#include <fstream>
// #include <sstream>

// #include <vector>

// #include <algorithm>
// #include <map>
// #include <iterator>
// #include <numeric>

#include <regex>


using namespace std;

int decode(std::string value);

#define SECOND_STAR

int main(int argc, char *argv[])
{
	string buffer;
		
	std::ifstream infile(argv[1]);

	int sum = 0;

	do
	{
		int num = 0;
		getline(infile, buffer);
#ifdef SECOND_STAR
		std::regex firstNumber("^.*?([0-9]|one|two|three|four|five|six|seven|eight|nine).*");
		std::regex lastNumber(".*([0-9]|one|two|three|four|five|six|seven|eight|nine).*?$");
#else
		std::regex firstNumber("^.*?([0-9]).*");
		std::regex lastNumber(".*([0-9]).*?$");
#endif
		
		std::smatch firstmatch;
		std::smatch lastmatch;
		std::regex_match(buffer, firstmatch, firstNumber);
		std::regex_match(buffer, lastmatch, lastNumber);

		if(firstmatch[0].length() > 0)
		{
			num = 10 * decode(firstmatch[1]) + decode(lastmatch[1]);
			sum += num;
		}
	} while (!infile.eof());
	
	cout << "Sum of all decoded numbers is : " << sum << endl;
	
	return 0;
}

int decode(std::string value)
{
	if(value == "one" || value == "1")
		return 1;
	else if(value == "two" || value == "2")
		return 2;
	else if(value == "three" || value == "3")
		return 3;
	else if(value == "four" || value == "4")
		return 4;
	else if(value == "five" || value == "5")
		return 5;
	else if(value == "six" || value == "6")
		return 6;
	else if(value == "seven" || value == "7")
		return 7;
	else if(value == "eight" || value == "8")
		return 8;
	else if(value == "nine" || value == "9")
		return 9;
	else
		return 0;
}
